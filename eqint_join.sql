﻿--select * from test

with one as (
	select min(disttwo), max(disttwo), 9 intervals from grids.island_hexagon_small_with_parishes   ),
two as (
	select *, @(max-min) rng, ceil(@(max-min) / intervals::float)::int bkt from one),
three as (
	select generate_series(min::int, max::int,bkt) gs from two),
four as (
	select  gs lwr, gs + two.bkt upr from three, two)

select lwr, ' ≤ x < ' as " ≤ x < ", upr/*,  count(id)*/   from four
/*inner join grids.island_hexagon_small_with_parishes on disttwo >= lwr AND disttwo < upr*/

group by lwr,upr
order by lwr 

/* sql to creae png file for ge */

st_asraster(geom,50.0,50.0,'{8BUI,8BUI,8BUI,8BUI}'::text[],'{10,15,200,0}'::int[], '{0,0,0,0}'::int[])
st_asraster(geom,10.0,10.0,'{8BUI,8BUI,8BUI,8BUI}'::text[],'{200,250,240,100}'::int[], '{0,0,0,0}'::int[])
            ^     ^          ^ 				    ^                           ^
    geometry       scalex/y   four bands RGBA		     Value for raster		 no data values 
