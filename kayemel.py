import random
colours = {'indigo': '82004B', 'gold': '00D7FF', 'firebrick': '2222B2', 'indianred': '5C5CCD',
           'yellow': '00FFFF', 'darkolivegreen': '2F6B55', 'darkseagreen': '8FBC8F', 'mediumvioletred': '8515C7',
           'mediumorchid': 'D355BA', 'chartreuse': '00FF7F', 'mediumslateblue': 'EE687B', 'black': '000000',
           'springgreen': '7FFF00', 'orange': '00A5FF', 'lightsalmon': '7AA0FF', 'brown': '2A2AA5',
           'turquoise': 'D0E040', 'lightseagreen': 'AAB220', 'cyan': 'FFFF00', 'silver': 'C0C0C0',
           'skyblue': 'EBCE87', 'gray': '808080', 'darkturquoise': 'D1CE00', 'goldenrod': '20A5DA',
           'darkgreen': '006400', 'darkviolet': 'D30094', 'darkgray': 'A9A9A9', 'lightpink': 'C1B6FF',
           'teal': '808000', 'darkmagenta': '8B008B', 'lightgoldenrodyellow': 'D2FAFA', 'lavender': 'FAE6E6',
           'yellowgreen': '32CD9A', 'thistle': 'D8BFD8', 'violet': 'EE82EE', 'navy': '800000',
           'orchid': 'D670DA', 'blue': 'FF0000', 'ghostwhite': 'FFF8F8', 'honeydew': 'F0FFF0',
           'cornflowerblue': 'ED9564', 'darkblue': '8B0000', 'darkkhaki': '6BB7BD', 'slategray': '908070',
           'cornsilk': 'DCF8FF', 'red': '0000FF', 'bisque': 'C4E4FF', 'darkcyan': '8B8B00', 'khaki': '8CE6F0',
           'wheat': 'B3DEF5', 'deepskyblue': 'FFBF00', 'darkred': '00008B', 'steelblue': 'B48246',
           'aliceblue': 'FFF8F0', 'gainsboro': 'DCDCDC', 'mediumturquoise': 'CCD148', 'floralwhite': 'F0FAFF',
           'plum': 'DDA0DD', 'lightgrey': 'D3D3D3', 'lightcyan': 'FFFFE0', 'darksalmon': '7A96E9',
           'beige': 'DCF5F5', 'azure': 'FFFFF0', 'lightsteelblue': 'DEC4B0', 'greenyellow': '2FFFAD',
           'royalblue': 'E16941', 'olivedrab': '238E6B', 'sienna': '2D52A0', 'mediumpurple': 'DB7093',
           'lightcoral': '8080F0', 'orangered': '0045FF', 'navajowhite': 'ADDEFF', 'lime': '00FF00',
           'palegreen': '98FB98', 'mistyrose': 'E1E4FF', 'seashell': 'EEF5FF', 'mediumspringgreen': '9AFA00',
           'fuchsia': 'FF00FF', 'papayawhip': 'D5EFFF', 'blanchedalmond': 'CDEBFF', 'lemonchiffon': 'CDFAFF',
           'peru': '3F85CD', 'aquamarine': 'D4FF7F', 'white': 'FFFFFF', 'darkslategray': '4F4F2F',
           'ivory': 'F0FFFF', 'darkgoldenrod': '0B86B8', 'lawngreen': '00FC7C', 'burlywood': '87B8DE',
           'crimson': '3C14DC', 'forestgreen': '228B22', 'slateblue': 'CD5A6A', 'olive': '008080',
           'mintcream': 'FAFFF5', 'antiquewhite': 'D7EBFA', 'darkorange': '008CFF', 'hotpink': 'B469FF',
           'moccasin': 'B5E4FF', 'limegreen': '32CD32', 'saddlebrown': '13458B', 'darkslateblue': '8B3D48',
           'lightskyblue': 'FACE87', 'deeppink': '9314FF', 'coral': '507FFF', 'aqua': 'FFFF00',
           'dodgerblue': 'FF901E', 'maroon': '000080', 'lightgreen': '90EE90', 'sandybrown': '60A4F4',
           'tan': '8CB4D2', 'magenta': 'FF00FF', 'rosybrown': '8F8FBC', 'pink': 'CBC0FF', 'lightblue': 'E6D8AD',
           'palevioletred': '9370DB', 'mediumseagreen': '71B33C', 'linen': 'E6F0FA', 'dimgray': '696969',
           'powderblue': 'E6E0B0', 'seagreen': '578B2E', 'snow': 'FAFAFF', 'mediumblue': 'CD0000',
           'midnightblue': '701919', 'paleturquoise': 'EEEEAF', 'palegoldenrod': 'AAE8EE',
           'whitesmoke': 'F5F5F5', 'darkorchid': 'CC3299', 'salmon': '7280FA', 'lightslategray': '998877',
           'oldlace': 'E6F5FD', 'chocolate': '1E69D2', 'tomato': '4763FF', 'cadetblue': 'A09E5F',
           'lightyellow': 'E0FFFF', 'lavenderblush': 'F5F0FF', 'purple': '800080',
           'mediumaquamarine': 'AACD66', 'green': '008000', 'blueviolet': 'E22B8A', 'peachpuff': 'B9DAFF'}


def alphaperc(ap):
    '''Returns a hex percentage of the given value'''
    if ap < 0 or ap > 100:
        return 'ff'
    else:
        return hex(int(ap/100 *255))[2:].rjust(2,'0')

def colourandom():
    '''Returns random colour'''
    return random.choice(list(colours.keys()))
    
def colour(colourtext = None, alpha = 95):
    if colourtext == None:
        return '<color>#{0}{1}</color>'.format(alphaperc(alpha),colours[colourandom()])

    if colourtext[0] == '#' and len(colourtext) == 7:
        return '<color>{0}</color>'.fomat(colourtext)
    else:
	try:
	  return '<color>#{0}{1}</color>'.format(alphaperc(alpha),colours[colourtext])
	except KeyError:
	  return '<color>#ffffffff</color>'

def style(intxt):
    '''i:donut:grey:40 I:donut:orange:99:1'''
    styles = dict()
    for q in intxt.split(' '):
        styles[q.split(':')[0]] = q.split(':')[1:]
    print(styles)


ex ='''  <IconStyle>
      <scale>1.125</scale>
      <color>#f25cbc5c</color>
      <Icon>
        <href>http://maps.google.com/mapfiles/kml/shapes/triangle.png</href>
      </Icon>
    </IconStyle>'''
