--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.3
-- Dumped by pg_dump version 9.1.3
-- Started on 2012-10-05 19:11:20

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 39 (class 2615 OID 2316177)
-- Name: kml; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA kml;


SET search_path = kml, pg_catalog;

--
-- TOC entry 688 (class 1255 OID 120039145)
-- Dependencies: 39
-- Name: colour(); Type: FUNCTION; Schema: kml; Owner: -
--

CREATE FUNCTION colour() RETURNS xml
    LANGUAGE sql
    AS $$
select xmlforest('#'||lpad(to_hex((random()* 255)::int),'2','0')||lpad(to_hex((random()* 255)::int),'2','0')||lpad(to_hex((random()* 255)::int),'2','0')||lpad(to_hex((random()* 255)::int),'2','0') as color)
$$;


--
-- TOC entry 689 (class 1255 OID 120039146)
-- Dependencies: 39
-- Name: colour(text); Type: FUNCTION; Schema: kml; Owner: -
--

CREATE FUNCTION colour(text) RETURNS xml
    LANGUAGE sql
    AS $_$
with colourlist as
	(select '"red"=>"0000FF", "tan"=>"8CB4D2", "aqua"=>"FFFF00", "blue"=>"FF0000", "cyan"=>"FFFF00", "gold"=>"00D7FF", "gray"=>"808080", "lime"=>"00FF00", "navy"=>"800000", "peru"=>"3F85CD", "pink"=>"CBC0FF", "plum"=>"DDA0DD", "snow"=>"FAFAFF", "teal"=>"808000", "azure"=>"FFFFF0", "beige"=>"DCF5F5", "black"=>"000000", "brown"=>"2A2AA5", "coral"=>"507FFF", "green"=>"008000", "ivory"=>"F0FFFF", "khaki"=>"8CE6F0", "linen"=>"E6F0FA", "olive"=>"008080", "wheat"=>"B3DEF5", "white"=>"FFFFFF", "bisque"=>"C4E4FF", "indigo"=>"82004B", "maroon"=>"000080", "orange"=>"00A5FF", "orchid"=>"D670DA", "purple"=>"800080", "salmon"=>"7280FA", "sienna"=>"2D52A0", "silver"=>"C0C0C0", "tomato"=>"4763FF", "violet"=>"EE82EE", "yellow"=>"00FFFF", "crimson"=>"3C14DC", "darkred"=>"00008B", "dimgray"=>"696969", "fuchsia"=>"FF00FF", "hotpink"=>"B469FF", "magenta"=>"FF00FF", "oldlace"=>"E6F5FD", "skyblue"=>"EBCE87", "thistle"=>"D8BFD8", "cornsilk"=>"DCF8FF", "darkblue"=>"8B0000", "darkcyan"=>"8B8B00", "darkgray"=>"A9A9A9", "deeppink"=>"9314FF", "honeydew"=>"F0FFF0", "lavender"=>"FAE6E6", "moccasin"=>"B5E4FF", "seagreen"=>"578B2E", "seashell"=>"EEF5FF", "aliceblue"=>"FFF8F0", "burlywood"=>"87B8DE", "cadetblue"=>"A09E5F", "chocolate"=>"1E69D2", "darkgreen"=>"006400", "darkkhaki"=>"6BB7BD", "firebrick"=>"2222B2", "gainsboro"=>"DCDCDC", "goldenrod"=>"20A5DA", "indianred"=>"5C5CCD", "lawngreen"=>"00FC7C", "lightblue"=>"E6D8AD", "lightcyan"=>"FFFFE0", "lightgrey"=>"D3D3D3", "lightpink"=>"C1B6FF", "limegreen"=>"32CD32", "mintcream"=>"FAFFF5", "mistyrose"=>"E1E4FF", "olivedrab"=>"238E6B", "orangered"=>"0045FF", "palegreen"=>"98FB98", "peachpuff"=>"B9DAFF", "rosybrown"=>"8F8FBC", "royalblue"=>"E16941", "slateblue"=>"CD5A6A", "slategray"=>"908070", "steelblue"=>"B48246", "turquoise"=>"D0E040", "aquamarine"=>"D4FF7F", "blueviolet"=>"E22B8A", "chartreuse"=>"00FF7F", "darkorange"=>"008CFF", "darkorchid"=>"CC3299", "darksalmon"=>"7A96E9", "darkviolet"=>"D30094", "dodgerblue"=>"FF901E", "ghostwhite"=>"FFF8F8", "lightcoral"=>"8080F0", "lightgreen"=>"90EE90", "mediumblue"=>"CD0000", "papayawhip"=>"D5EFFF", "powderblue"=>"E6E0B0", "sandybrown"=>"60A4F4", "whitesmoke"=>"F5F5F5", "darkmagenta"=>"8B008B", "deepskyblue"=>"FFBF00", "floralwhite"=>"F0FAFF", "forestgreen"=>"228B22", "greenyellow"=>"2FFFAD", "lightsalmon"=>"7AA0FF", "lightyellow"=>"E0FFFF", "navajowhite"=>"ADDEFF", "saddlebrown"=>"13458B", "springgreen"=>"7FFF00", "yellowgreen"=>"32CD9A", "antiquewhite"=>"D7EBFA", "darkseagreen"=>"8FBC8F", "lemonchiffon"=>"CDFAFF", "lightskyblue"=>"FACE87", "mediumorchid"=>"D355BA", "mediumpurple"=>"DB7093", "midnightblue"=>"701919", "darkgoldenrod"=>"0B86B8", "darkslateblue"=>"8B3D48", "darkslategray"=>"4F4F2F", "darkturquoise"=>"D1CE00", "lavenderblush"=>"F5F0FF", "lightseagreen"=>"AAB220", "palegoldenrod"=>"AAE8EE", "paleturquoise"=>"EEEEAF", "palevioletred"=>"9370DB", "blanchedalmond"=>"CDEBFF", "cornflowerblue"=>"ED9564", "darkolivegreen"=>"2F6B55", "lightslategray"=>"998877", "lightsteelblue"=>"DEC4B0", "mediumseagreen"=>"71B33C", "mediumslateblue"=>"EE687B", "mediumturquoise"=>"CCD148", "mediumvioletred"=>"8515C7", "mediumaquamarine"=>"AACD66", "mediumspringgreen"=>"9AFA00", "lightgoldenrodyellow"=>"D2FAFA"'::hstore h)
	select  
	CASE when substr($1,1,1) = '#' THEN xmlforest($1 as color)
	ELSE
	xmlforest(coalesce('#ff'||( h->lower($1)),'#ffffffff') as color) END
	from colourlist  
	

$_$;


--
-- TOC entry 691 (class 1255 OID 120039155)
-- Dependencies: 39
-- Name: colour(integer); Type: FUNCTION; Schema: kml; Owner: -
--

CREATE FUNCTION colour(alphaperc integer) RETURNS xml
    LANGUAGE sql
    AS $_$
select CASE when $1 > 100 OR $1 < 0 THEN
	xmlforest('#ff'||lpad(to_hex((random()* 255)::int),'2','0')||lpad(to_hex((random()* 255)::int),'2','0')||lpad(to_hex((random()* 255)::int),'2','0') as color)
	ELSE
	xmlforest('#'||lpad(to_hex(floor($1/100.0 * 255)::int),2,'0')||lpad(to_hex((random()* 255)::int),'2','0')||lpad(to_hex((random()* 255)::int),'2','0')||lpad(to_hex((random()* 255)::int),'2','0') as color)
	END
$_$;


--
-- TOC entry 690 (class 1255 OID 120039152)
-- Dependencies: 39
-- Name: colour(text, integer); Type: FUNCTION; Schema: kml; Owner: -
--

CREATE FUNCTION colour(text, integer) RETURNS xml
    LANGUAGE sql
    AS $_$
with colourlist as
	(select '"red"=>"0000FF", "tan"=>"8CB4D2", "aqua"=>"FFFF00", "blue"=>"FF0000", "cyan"=>"FFFF00", "gold"=>"00D7FF", "gray"=>"808080", "lime"=>"00FF00", "navy"=>"800000", "peru"=>"3F85CD", "pink"=>"CBC0FF", "plum"=>"DDA0DD", "snow"=>"FAFAFF", "teal"=>"808000", "azure"=>"FFFFF0", "beige"=>"DCF5F5", "black"=>"000000", "brown"=>"2A2AA5", "coral"=>"507FFF", "green"=>"008000", "ivory"=>"F0FFFF", "khaki"=>"8CE6F0", "linen"=>"E6F0FA", "olive"=>"008080", "wheat"=>"B3DEF5", "white"=>"FFFFFF", "bisque"=>"C4E4FF", "indigo"=>"82004B", "maroon"=>"000080", "orange"=>"00A5FF", "orchid"=>"D670DA", "purple"=>"800080", "salmon"=>"7280FA", "sienna"=>"2D52A0", "silver"=>"C0C0C0", "tomato"=>"4763FF", "violet"=>"EE82EE", "yellow"=>"00FFFF", "crimson"=>"3C14DC", "darkred"=>"00008B", "dimgray"=>"696969", "fuchsia"=>"FF00FF", "hotpink"=>"B469FF", "magenta"=>"FF00FF", "oldlace"=>"E6F5FD", "skyblue"=>"EBCE87", "thistle"=>"D8BFD8", "cornsilk"=>"DCF8FF", "darkblue"=>"8B0000", "darkcyan"=>"8B8B00", "darkgray"=>"A9A9A9", "deeppink"=>"9314FF", "honeydew"=>"F0FFF0", "lavender"=>"FAE6E6", "moccasin"=>"B5E4FF", "seagreen"=>"578B2E", "seashell"=>"EEF5FF", "aliceblue"=>"FFF8F0", "burlywood"=>"87B8DE", "cadetblue"=>"A09E5F", "chocolate"=>"1E69D2", "darkgreen"=>"006400", "darkkhaki"=>"6BB7BD", "firebrick"=>"2222B2", "gainsboro"=>"DCDCDC", "goldenrod"=>"20A5DA", "indianred"=>"5C5CCD", "lawngreen"=>"00FC7C", "lightblue"=>"E6D8AD", "lightcyan"=>"FFFFE0", "lightgrey"=>"D3D3D3", "lightpink"=>"C1B6FF", "limegreen"=>"32CD32", "mintcream"=>"FAFFF5", "mistyrose"=>"E1E4FF", "olivedrab"=>"238E6B", "orangered"=>"0045FF", "palegreen"=>"98FB98", "peachpuff"=>"B9DAFF", "rosybrown"=>"8F8FBC", "royalblue"=>"E16941", "slateblue"=>"CD5A6A", "slategray"=>"908070", "steelblue"=>"B48246", "turquoise"=>"D0E040", "aquamarine"=>"D4FF7F", "blueviolet"=>"E22B8A", "chartreuse"=>"00FF7F", "darkorange"=>"008CFF", "darkorchid"=>"CC3299", "darksalmon"=>"7A96E9", "darkviolet"=>"D30094", "dodgerblue"=>"FF901E", "ghostwhite"=>"FFF8F8", "lightcoral"=>"8080F0", "lightgreen"=>"90EE90", "mediumblue"=>"CD0000", "papayawhip"=>"D5EFFF", "powderblue"=>"E6E0B0", "sandybrown"=>"60A4F4", "whitesmoke"=>"F5F5F5", "darkmagenta"=>"8B008B", "deepskyblue"=>"FFBF00", "floralwhite"=>"F0FAFF", "forestgreen"=>"228B22", "greenyellow"=>"2FFFAD", "lightsalmon"=>"7AA0FF", "lightyellow"=>"E0FFFF", "navajowhite"=>"ADDEFF", "saddlebrown"=>"13458B", "springgreen"=>"7FFF00", "yellowgreen"=>"32CD9A", "antiquewhite"=>"D7EBFA", "darkseagreen"=>"8FBC8F", "lemonchiffon"=>"CDFAFF", "lightskyblue"=>"FACE87", "mediumorchid"=>"D355BA", "mediumpurple"=>"DB7093", "midnightblue"=>"701919", "darkgoldenrod"=>"0B86B8", "darkslateblue"=>"8B3D48", "darkslategray"=>"4F4F2F", "darkturquoise"=>"D1CE00", "lavenderblush"=>"F5F0FF", "lightseagreen"=>"AAB220", "palegoldenrod"=>"AAE8EE", "paleturquoise"=>"EEEEAF", "palevioletred"=>"9370DB", "blanchedalmond"=>"CDEBFF", "cornflowerblue"=>"ED9564", "darkolivegreen"=>"2F6B55", "lightslategray"=>"998877", "lightsteelblue"=>"DEC4B0", "mediumseagreen"=>"71B33C", "mediumslateblue"=>"EE687B", "mediumturquoise"=>"CCD148", "mediumvioletred"=>"8515C7", "mediumaquamarine"=>"AACD66", "mediumspringgreen"=>"9AFA00", "lightgoldenrodyellow"=>"D2FAFA"'::hstore h)
	select  
	  CASE when $2 > 100 OR $2 < 0 THEN xmlforest('#ffffffff' as color)
	  ELSE xmlforest(coalesce('#'||
	  lpad(to_hex(floor($2/100.0 * 255)::int),2,'0')
	  ||( h->lower($1)),'#ffffffff') as color) END
	from colourlist  
	

$_$;


--
-- TOC entry 696 (class 1255 OID 120039090)
-- Dependencies: 39
-- Name: documentfinal(xml); Type: FUNCTION; Schema: kml; Owner: -
--

CREATE FUNCTION documentfinal(xml) RETURNS xml
    LANGUAGE sql
    AS $_$
select xmlelement(name kml, xmlattributes('http://www.opengis.net/kml/2.2' as xmlns), xmlelement(name "Document", $1 ))
$_$;


--
-- TOC entry 693 (class 1255 OID 120039067)
-- Dependencies: 39 1984
-- Name: placemark(public.geometry); Type: FUNCTION; Schema: kml; Owner: -
--

CREATE FUNCTION placemark(public.geometry) RETURNS xml
    LANGUAGE sql
    AS $_$
select xmlelement(name "Placemark" ,st_askml($1)::xml)
$_$;


--
-- TOC entry 687 (class 1255 OID 120039027)
-- Dependencies: 39 1984
-- Name: placemark(public.geometry, text[]); Type: FUNCTION; Schema: kml; Owner: -
--

CREATE FUNCTION placemark(public.geometry, VARIADIC text[]) RETURNS xml
    LANGUAGE sql
    AS $_$
select xmlelement(name "Placemark" ,xmlforest($2[1] as name, array_to_string($2[2:array_length($2,1)],E'\n') as description), st_askml($1)::xml)
$_$;


--
-- TOC entry 692 (class 1255 OID 120039057)
-- Dependencies: 39
-- Name: precon(xml, xml); Type: FUNCTION; Schema: kml; Owner: -
--

CREATE FUNCTION precon(xml, xml) RETURNS xml
    LANGUAGE sql
    AS $_$
select xmlconcat($1,$2)
$_$;


--
-- TOC entry 793 (class 1255 OID 120039130)
-- Dependencies: 39
-- Name: style(text[]); Type: FUNCTION; Schema: kml; Owner: -
--

CREATE FUNCTION style(VARIADIC text[]) RETURNS xml
    LANGUAGE sql
    AS $_$
select CASE WHEN substr($1[1],1,1) = 'i' THEN xmlelement(name "Icon", xmlelement(name href, format('http://maps.google.com/mapfiles/kml/shapes/%s.png',ltrim($1[1],'i:'))))
	else xmlcomment('olé!!!') END
	
$_$;


--
-- TOC entry 702 (class 1255 OID 120039105)
-- Dependencies: 39
-- Name: style_arbtry(); Type: FUNCTION; Schema: kml; Owner: -
--

CREATE FUNCTION style_arbtry() RETURNS xml
    LANGUAGE sql
    AS $$ select xmlelement(name "Style",xmlelement(name "PolyStyle",xmlforest(1 as fill, 1 as outline,kml.colour_arbtry() as color )))
$$;


--
-- TOC entry 783 (class 1255 OID 120039107)
-- Dependencies: 39
-- Name: style_arbtry(integer); Type: FUNCTION; Schema: kml; Owner: -
--

CREATE FUNCTION style_arbtry(alphaperc integer) RETURNS xml
    LANGUAGE sql
    AS $_$ select xmlelement(name "Style",xmlelement(name "PolyStyle",xmlforest(1 as fill, 1 as outline,kml.colour_arbtry($1) as color )))
$_$;


--
-- TOC entry 686 (class 1255 OID 120039136)
-- Dependencies: 39
-- Name: styleicon(text); Type: FUNCTION; Schema: kml; Owner: -
--

CREATE FUNCTION styleicon(text) RETURNS xml
    LANGUAGE sql
    AS $_$
with one as (select string_to_array($1,':') o)
select 
  xmlelement(name "IconStyle",
  xmlforest( o[4] as scale),kml.colour(o[3]),
  xmlelement(name "Icon", xmlforest(format('http://maps.google.com/mapfiles/kml/shapes/%s.png',o[2]) as href))
  )

from one 
$_$;


--
-- TOC entry 2725 (class 1255 OID 120039131)
-- Dependencies: 696 39 692
-- Name: document(xml); Type: AGGREGATE; Schema: kml; Owner: -
--

CREATE AGGREGATE document(xml) (
    SFUNC = precon,
    STYPE = xml,
    FINALFUNC = documentfinal
);


--
-- TOC entry 2726 (class 1255 OID 120039058)
-- Dependencies: 39 692
-- Name: documentagg(xml); Type: AGGREGATE; Schema: kml; Owner: -
--

CREATE AGGREGATE documentagg(xml) (
    SFUNC = precon,
    STYPE = xml,
    INITCOND = ''
);


-- Completed on 2012-10-05 19:11:23

--
-- PostgreSQL database dump complete
--

