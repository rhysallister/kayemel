--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.3
-- Dumped by pg_dump version 9.2.0
-- Started on 2012-10-06 18:34:50 EST

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 17 (class 2615 OID 43856)
-- Name: kml; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA kml;


ALTER SCHEMA kml OWNER TO postgres;

SET search_path = kml, pg_catalog;

--
-- TOC entry 1219 (class 1255 OID 43857)
-- Name: colour(); Type: FUNCTION; Schema: kml; Owner: postgres
--

CREATE FUNCTION colour() RETURNS xml
    LANGUAGE sql
    AS $$
select xmlforest('#'||lpad(to_hex((random()* 255)::int),'2','0')||lpad(to_hex((random()* 255)::int),'2','0')||lpad(to_hex((random()* 255)::int),'2','0')||lpad(to_hex((random()* 255)::int),'2','0') as color)
$$;


ALTER FUNCTION kml.colour() OWNER TO postgres;

--
-- TOC entry 3227 (class 0 OID 0)
-- Dependencies: 1219
-- Name: FUNCTION colour(); Type: COMMENT; Schema: kml; Owner: postgres
--

COMMENT ON FUNCTION colour() IS 'Function returns a random kml colour. Including random alpha. So it could be totally transparent.';


--
-- TOC entry 1220 (class 1255 OID 43858)
-- Name: colour(text); Type: FUNCTION; Schema: kml; Owner: postgres
--

CREATE FUNCTION colour(text) RETURNS xml
    LANGUAGE sql
    AS $_$
with colourlist as
	(select '"red"=>"0000FF", "tan"=>"8CB4D2", "aqua"=>"FFFF00", "blue"=>"FF0000", "cyan"=>"FFFF00", "gold"=>"00D7FF", "gray"=>"808080", "lime"=>"00FF00", "navy"=>"800000", "peru"=>"3F85CD", "pink"=>"CBC0FF", "plum"=>"DDA0DD", "snow"=>"FAFAFF", "teal"=>"808000", "azure"=>"FFFFF0", "beige"=>"DCF5F5", "black"=>"000000", "brown"=>"2A2AA5", "coral"=>"507FFF", "green"=>"008000", "ivory"=>"F0FFFF", "khaki"=>"8CE6F0", "linen"=>"E6F0FA", "olive"=>"008080", "wheat"=>"B3DEF5", "white"=>"FFFFFF", "bisque"=>"C4E4FF", "indigo"=>"82004B", "maroon"=>"000080", "orange"=>"00A5FF", "orchid"=>"D670DA", "purple"=>"800080", "salmon"=>"7280FA", "sienna"=>"2D52A0", "silver"=>"C0C0C0", "tomato"=>"4763FF", "violet"=>"EE82EE", "yellow"=>"00FFFF", "crimson"=>"3C14DC", "darkred"=>"00008B", "dimgray"=>"696969", "fuchsia"=>"FF00FF", "hotpink"=>"B469FF", "magenta"=>"FF00FF", "oldlace"=>"E6F5FD", "skyblue"=>"EBCE87", "thistle"=>"D8BFD8", "cornsilk"=>"DCF8FF", "darkblue"=>"8B0000", "darkcyan"=>"8B8B00", "darkgray"=>"A9A9A9", "deeppink"=>"9314FF", "honeydew"=>"F0FFF0", "lavender"=>"FAE6E6", "moccasin"=>"B5E4FF", "seagreen"=>"578B2E", "seashell"=>"EEF5FF", "aliceblue"=>"FFF8F0", "burlywood"=>"87B8DE", "cadetblue"=>"A09E5F", "chocolate"=>"1E69D2", "darkgreen"=>"006400", "darkkhaki"=>"6BB7BD", "firebrick"=>"2222B2", "gainsboro"=>"DCDCDC", "goldenrod"=>"20A5DA", "indianred"=>"5C5CCD", "lawngreen"=>"00FC7C", "lightblue"=>"E6D8AD", "lightcyan"=>"FFFFE0", "lightgrey"=>"D3D3D3", "lightpink"=>"C1B6FF", "limegreen"=>"32CD32", "mintcream"=>"FAFFF5", "mistyrose"=>"E1E4FF", "olivedrab"=>"238E6B", "orangered"=>"0045FF", "palegreen"=>"98FB98", "peachpuff"=>"B9DAFF", "rosybrown"=>"8F8FBC", "royalblue"=>"E16941", "slateblue"=>"CD5A6A", "slategray"=>"908070", "steelblue"=>"B48246", "turquoise"=>"D0E040", "aquamarine"=>"D4FF7F", "blueviolet"=>"E22B8A", "chartreuse"=>"00FF7F", "darkorange"=>"008CFF", "darkorchid"=>"CC3299", "darksalmon"=>"7A96E9", "darkviolet"=>"D30094", "dodgerblue"=>"FF901E", "ghostwhite"=>"FFF8F8", "lightcoral"=>"8080F0", "lightgreen"=>"90EE90", "mediumblue"=>"CD0000", "papayawhip"=>"D5EFFF", "powderblue"=>"E6E0B0", "sandybrown"=>"60A4F4", "whitesmoke"=>"F5F5F5", "darkmagenta"=>"8B008B", "deepskyblue"=>"FFBF00", "floralwhite"=>"F0FAFF", "forestgreen"=>"228B22", "greenyellow"=>"2FFFAD", "lightsalmon"=>"7AA0FF", "lightyellow"=>"E0FFFF", "navajowhite"=>"ADDEFF", "saddlebrown"=>"13458B", "springgreen"=>"7FFF00", "yellowgreen"=>"32CD9A", "antiquewhite"=>"D7EBFA", "darkseagreen"=>"8FBC8F", "lemonchiffon"=>"CDFAFF", "lightskyblue"=>"FACE87", "mediumorchid"=>"D355BA", "mediumpurple"=>"DB7093", "midnightblue"=>"701919", "darkgoldenrod"=>"0B86B8", "darkslateblue"=>"8B3D48", "darkslategray"=>"4F4F2F", "darkturquoise"=>"D1CE00", "lavenderblush"=>"F5F0FF", "lightseagreen"=>"AAB220", "palegoldenrod"=>"AAE8EE", "paleturquoise"=>"EEEEAF", "palevioletred"=>"9370DB", "blanchedalmond"=>"CDEBFF", "cornflowerblue"=>"ED9564", "darkolivegreen"=>"2F6B55", "lightslategray"=>"998877", "lightsteelblue"=>"DEC4B0", "mediumseagreen"=>"71B33C", "mediumslateblue"=>"EE687B", "mediumturquoise"=>"CCD148", "mediumvioletred"=>"8515C7", "mediumaquamarine"=>"AACD66", "mediumspringgreen"=>"9AFA00", "lightgoldenrodyellow"=>"D2FAFA"'::hstore h)
	select  
	CASE when substr($1,1,1) = '#' THEN xmlforest($1 as color)
	ELSE
	xmlforest(coalesce('#ff'||( h->lower($1)),'#ffffffff') as color) END
	from colourlist  
	

$_$;


ALTER FUNCTION kml.colour(text) OWNER TO postgres;

--
-- TOC entry 1221 (class 1255 OID 43860)
-- Name: colour(integer); Type: FUNCTION; Schema: kml; Owner: postgres
--

CREATE FUNCTION colour(alphaperc integer) RETURNS xml
    LANGUAGE sql
    AS $_$
select CASE when $1 > 100 OR $1 < 0 THEN
	xmlforest('#ff'||lpad(to_hex((random()* 255)::int),'2','0')||lpad(to_hex((random()* 255)::int),'2','0')||lpad(to_hex((random()* 255)::int),'2','0') as color)
	ELSE
	xmlforest('#'||lpad(to_hex(floor($1/100.0 * 255)::int),2,'0')||lpad(to_hex((random()* 255)::int),'2','0')||lpad(to_hex((random()* 255)::int),'2','0')||lpad(to_hex((random()* 255)::int),'2','0') as color)
	END
$_$;


ALTER FUNCTION kml.colour(alphaperc integer) OWNER TO postgres;

--
-- TOC entry 1222 (class 1255 OID 43861)
-- Name: colour(text, integer); Type: FUNCTION; Schema: kml; Owner: postgres
--

CREATE FUNCTION colour(text, integer) RETURNS xml
    LANGUAGE sql
    AS $_$
with colourlist as
	(select '"red"=>"0000FF", "tan"=>"8CB4D2", "aqua"=>"FFFF00", "blue"=>"FF0000", "cyan"=>"FFFF00", "gold"=>"00D7FF", "gray"=>"808080", "lime"=>"00FF00", "navy"=>"800000", "peru"=>"3F85CD", "pink"=>"CBC0FF", "plum"=>"DDA0DD", "snow"=>"FAFAFF", "teal"=>"808000", "azure"=>"FFFFF0", "beige"=>"DCF5F5", "black"=>"000000", "brown"=>"2A2AA5", "coral"=>"507FFF", "green"=>"008000", "ivory"=>"F0FFFF", "khaki"=>"8CE6F0", "linen"=>"E6F0FA", "olive"=>"008080", "wheat"=>"B3DEF5", "white"=>"FFFFFF", "bisque"=>"C4E4FF", "indigo"=>"82004B", "maroon"=>"000080", "orange"=>"00A5FF", "orchid"=>"D670DA", "purple"=>"800080", "salmon"=>"7280FA", "sienna"=>"2D52A0", "silver"=>"C0C0C0", "tomato"=>"4763FF", "violet"=>"EE82EE", "yellow"=>"00FFFF", "crimson"=>"3C14DC", "darkred"=>"00008B", "dimgray"=>"696969", "fuchsia"=>"FF00FF", "hotpink"=>"B469FF", "magenta"=>"FF00FF", "oldlace"=>"E6F5FD", "skyblue"=>"EBCE87", "thistle"=>"D8BFD8", "cornsilk"=>"DCF8FF", "darkblue"=>"8B0000", "darkcyan"=>"8B8B00", "darkgray"=>"A9A9A9", "deeppink"=>"9314FF", "honeydew"=>"F0FFF0", "lavender"=>"FAE6E6", "moccasin"=>"B5E4FF", "seagreen"=>"578B2E", "seashell"=>"EEF5FF", "aliceblue"=>"FFF8F0", "burlywood"=>"87B8DE", "cadetblue"=>"A09E5F", "chocolate"=>"1E69D2", "darkgreen"=>"006400", "darkkhaki"=>"6BB7BD", "firebrick"=>"2222B2", "gainsboro"=>"DCDCDC", "goldenrod"=>"20A5DA", "indianred"=>"5C5CCD", "lawngreen"=>"00FC7C", "lightblue"=>"E6D8AD", "lightcyan"=>"FFFFE0", "lightgrey"=>"D3D3D3", "lightpink"=>"C1B6FF", "limegreen"=>"32CD32", "mintcream"=>"FAFFF5", "mistyrose"=>"E1E4FF", "olivedrab"=>"238E6B", "orangered"=>"0045FF", "palegreen"=>"98FB98", "peachpuff"=>"B9DAFF", "rosybrown"=>"8F8FBC", "royalblue"=>"E16941", "slateblue"=>"CD5A6A", "slategray"=>"908070", "steelblue"=>"B48246", "turquoise"=>"D0E040", "aquamarine"=>"D4FF7F", "blueviolet"=>"E22B8A", "chartreuse"=>"00FF7F", "darkorange"=>"008CFF", "darkorchid"=>"CC3299", "darksalmon"=>"7A96E9", "darkviolet"=>"D30094", "dodgerblue"=>"FF901E", "ghostwhite"=>"FFF8F8", "lightcoral"=>"8080F0", "lightgreen"=>"90EE90", "mediumblue"=>"CD0000", "papayawhip"=>"D5EFFF", "powderblue"=>"E6E0B0", "sandybrown"=>"60A4F4", "whitesmoke"=>"F5F5F5", "darkmagenta"=>"8B008B", "deepskyblue"=>"FFBF00", "floralwhite"=>"F0FAFF", "forestgreen"=>"228B22", "greenyellow"=>"2FFFAD", "lightsalmon"=>"7AA0FF", "lightyellow"=>"E0FFFF", "navajowhite"=>"ADDEFF", "saddlebrown"=>"13458B", "springgreen"=>"7FFF00", "yellowgreen"=>"32CD9A", "antiquewhite"=>"D7EBFA", "darkseagreen"=>"8FBC8F", "lemonchiffon"=>"CDFAFF", "lightskyblue"=>"FACE87", "mediumorchid"=>"D355BA", "mediumpurple"=>"DB7093", "midnightblue"=>"701919", "darkgoldenrod"=>"0B86B8", "darkslateblue"=>"8B3D48", "darkslategray"=>"4F4F2F", "darkturquoise"=>"D1CE00", "lavenderblush"=>"F5F0FF", "lightseagreen"=>"AAB220", "palegoldenrod"=>"AAE8EE", "paleturquoise"=>"EEEEAF", "palevioletred"=>"9370DB", "blanchedalmond"=>"CDEBFF", "cornflowerblue"=>"ED9564", "darkolivegreen"=>"2F6B55", "lightslategray"=>"998877", "lightsteelblue"=>"DEC4B0", "mediumseagreen"=>"71B33C", "mediumslateblue"=>"EE687B", "mediumturquoise"=>"CCD148", "mediumvioletred"=>"8515C7", "mediumaquamarine"=>"AACD66", "mediumspringgreen"=>"9AFA00", "lightgoldenrodyellow"=>"D2FAFA"'::hstore h)
	select  
	  CASE when $2 > 100 OR $2 < 0 THEN xmlforest('#ffffffff' as color)
	  ELSE xmlforest(coalesce('#'||
	  lpad(to_hex(floor($2/100.0 * 255)::int),2,'0')
	  ||( h->lower($1)),'#ffffffff') as color) END
	from colourlist  
	

$_$;


ALTER FUNCTION kml.colour(text, integer) OWNER TO postgres;

--
-- TOC entry 1223 (class 1255 OID 43863)
-- Name: documentfinal(xml); Type: FUNCTION; Schema: kml; Owner: postgres
--

CREATE FUNCTION documentfinal(xml) RETURNS xml
    LANGUAGE sql
    AS $_$
select xmlelement(name kml, xmlattributes('http://www.opengis.net/kml/2.2' as xmlns), xmlelement(name "Document", $1 ))
$_$;


ALTER FUNCTION kml.documentfinal(xml) OWNER TO postgres;

--
-- TOC entry 1224 (class 1255 OID 43864)
-- Name: placemark(public.geometry); Type: FUNCTION; Schema: kml; Owner: postgres
--

CREATE FUNCTION placemark(public.geometry) RETURNS xml
    LANGUAGE sql
    AS $_$
select xmlelement(name "Placemark" ,st_askml($1)::xml)
$_$;


ALTER FUNCTION kml.placemark(public.geometry) OWNER TO postgres;

--
-- TOC entry 1225 (class 1255 OID 43865)
-- Name: placemark(public.geometry, text[]); Type: FUNCTION; Schema: kml; Owner: postgres
--

CREATE FUNCTION placemark(public.geometry, VARIADIC text[]) RETURNS xml
    LANGUAGE sql
    AS $_$
select xmlelement(name "Placemark" ,xmlforest($2[1] as name, array_to_string($2[2:array_length($2,1)],E'\n') as description), st_askml($1)::xml)
$_$;


ALTER FUNCTION kml.placemark(public.geometry, VARIADIC text[]) OWNER TO postgres;

--
-- TOC entry 1228 (class 1255 OID 43873)
-- Name: placemark(public.geometry, xml, text[]); Type: FUNCTION; Schema: kml; Owner: postgres
--

CREATE FUNCTION placemark(public.geometry, xml, VARIADIC text[]) RETURNS xml
    LANGUAGE sql
    AS $_$
select xmlelement(name "Placemark" ,$2,xmlforest($3[1] as name, array_to_string($3[2:array_length($3,1)],E'\n') as description), st_askml($1)::xml)
$_$;


ALTER FUNCTION kml.placemark(public.geometry, xml, VARIADIC text[]) OWNER TO postgres;

--
-- TOC entry 1226 (class 1255 OID 43866)
-- Name: precon(xml, xml); Type: FUNCTION; Schema: kml; Owner: postgres
--

CREATE FUNCTION precon(xml, xml) RETURNS xml
    LANGUAGE sql
    AS $_$
select xmlconcat($1,$2)
$_$;


ALTER FUNCTION kml.precon(xml, xml) OWNER TO postgres;

--
-- TOC entry 1234 (class 1255 OID 43882)
-- Name: style(); Type: FUNCTION; Schema: kml; Owner: postgres
--

CREATE FUNCTION style() RETURNS xml
    LANGUAGE sql
    AS $$
select 
xmlelement(
  name "Style",
  kml.styleicon(),
  kml.stylepoly(),
  kml.styleline())
 $$;


ALTER FUNCTION kml.style() OWNER TO postgres;

--
-- TOC entry 1235 (class 1255 OID 43883)
-- Name: style(text); Type: FUNCTION; Schema: kml; Owner: postgres
--

CREATE FUNCTION style(text) RETURNS xml
    LANGUAGE sql
    AS $_$
with one as 
	(select $1::text o),
two as
	(select unnest(string_to_array(o,' ')) o from one)

select xmlelement(name "Style", xmlagg( CASE 
	WHEN substr(o,1,1) = 'p' THEN kml.stylepoly(o) 
	WHEN substr(o,1,1) = 'i' THEN kml.styleicon(o)
	WHEN substr(o,1,1) = 'l' THEN kml.styleline(o)
	END))
 from two
 $_$;


ALTER FUNCTION kml.style(text) OWNER TO postgres;

--
-- TOC entry 1229 (class 1255 OID 43874)
-- Name: styleicon(); Type: FUNCTION; Schema: kml; Owner: postgres
--

CREATE FUNCTION styleicon() RETURNS xml
    LANGUAGE sql
    AS $$
with one as 
  (select unnest('{donut,shaded_dot,open-diamond,polygon,placemark_square,placemark_circle,triangle,target,star}'::text[]) o order by random() limit 1)
select 
  xmlelement(name "IconStyle",
  xmlforest( 1.125 as scale),kml.colour(95),
  xmlelement(name "Icon", xmlforest(format('http://maps.google.com/mapfiles/kml/shapes/%s.png',o) as href))
  )

from one 
$$;


ALTER FUNCTION kml.styleicon() OWNER TO postgres;

--
-- TOC entry 1227 (class 1255 OID 43870)
-- Name: styleicon(text); Type: FUNCTION; Schema: kml; Owner: postgres
--

CREATE FUNCTION styleicon(text) RETURNS xml
    LANGUAGE sql
    AS $_$
with one as (select string_to_array($1,':') o)
select 
  xmlelement(name "IconStyle",
  xmlforest( o[4] as scale),kml.colour(o[3]),
  xmlelement(name "Icon", xmlforest(format('http://maps.google.com/mapfiles/kml/shapes/%s.png',o[2]) as href))
  )

from one 
$_$;


ALTER FUNCTION kml.styleicon(text) OWNER TO postgres;

--
-- TOC entry 1233 (class 1255 OID 43881)
-- Name: styleline(); Type: FUNCTION; Schema: kml; Owner: postgres
--

CREATE FUNCTION styleline() RETURNS xml
    LANGUAGE sql
    AS $$


select 
  xmlelement(
	name "LineStyle",
	kml.colour(95),
	xmlforest(1.75 AS width) 
	
  
  ) 
$$;


ALTER FUNCTION kml.styleline() OWNER TO postgres;

--
-- TOC entry 1232 (class 1255 OID 43879)
-- Name: styleline(text); Type: FUNCTION; Schema: kml; Owner: postgres
--

CREATE FUNCTION styleline(text) RETURNS xml
    LANGUAGE sql
    AS $_$

with one as (select string_to_array($1,':') o)
select 
  xmlelement(
	name "LineStyle",
	CASE WHEN o[3] IS NULL or o[3] !~ '[0-9]' THEN kml.colour(o[2]) ELSE kml.colour(o[2],o[3]::int) END,
	CASE WHEN 'px' ~any (o[3:5]) THEN xmlforest(o[array_upper(o,1) - 1] AS width) END
	
  
  ) from one

$_$;


ALTER FUNCTION kml.styleline(text) OWNER TO postgres;

--
-- TOC entry 1231 (class 1255 OID 43878)
-- Name: stylepoly(); Type: FUNCTION; Schema: kml; Owner: postgres
--

CREATE FUNCTION stylepoly() RETURNS xml
    LANGUAGE sql
    AS $$

select 
  xmlelement(
	name "PolyStyle",
	kml.colour(),
	xmlforest(1 AS outline,1 AS fill )
  
  ) 
$$;


ALTER FUNCTION kml.stylepoly() OWNER TO postgres;

--
-- TOC entry 1230 (class 1255 OID 43875)
-- Name: stylepoly(text); Type: FUNCTION; Schema: kml; Owner: postgres
--

CREATE FUNCTION stylepoly(text) RETURNS xml
    LANGUAGE sql
    AS $_$

with one as (select string_to_array($1,':') o)
select 
  xmlelement(
	name "PolyStyle",
	CASE WHEN o[3] IS NULL or o[3] !~ '[0-9]' THEN kml.colour(o[2]) ELSE kml.colour(o[2],o[3]::int) END,
	CASE WHEN 'o' =any (o[3:5]) THEN xmlforest(1 AS outline) END, 
	CASE WHEN 'f' =any (o[3:5]) THEN xmlforest(1 AS fill )END
  
  ) from one

$_$;


ALTER FUNCTION kml.stylepoly(text) OWNER TO postgres;

--
-- TOC entry 1741 (class 1255 OID 43871)
-- Name: document(xml); Type: AGGREGATE; Schema: kml; Owner: postgres
--

CREATE AGGREGATE document(xml) (
    SFUNC = precon,
    STYPE = xml,
    FINALFUNC = documentfinal
);


ALTER AGGREGATE kml.document(xml) OWNER TO postgres;

--
-- TOC entry 1742 (class 1255 OID 43872)
-- Name: documentagg(xml); Type: AGGREGATE; Schema: kml; Owner: postgres
--

CREATE AGGREGATE documentagg(xml) (
    SFUNC = precon,
    STYPE = xml,
    INITCOND = ''
);


ALTER AGGREGATE kml.documentagg(xml) OWNER TO postgres;

-- Completed on 2012-10-06 18:34:51 EST

--
-- PostgreSQL database dump complete
--

